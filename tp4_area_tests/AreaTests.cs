﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using tp4_area;

namespace tp4_area_tests
{
    [TestClass]
    public class AreaTests
    {
        [TestMethod]
        public void CalculateAreaSquare()
        {
            Assert.AreEqual(16, Program.CalculateAreaSquare(4));
            Assert.AreNotEqual(11, Program.CalculateAreaSquare(4));
            Assert.AreEqual(52.5625, Program.CalculateAreaSquare(7.25));
        }
        [TestMethod]
        public void CalculateAreaRectangle()
        {
            Assert.AreEqual(20, Program.CalculateAreaRectangle(10, 2));
            Assert.AreNotEqual(15, Program.CalculateAreaRectangle(7, 2));
            Assert.AreEqual(15, Program.CalculateAreaRectangle(6, 2.5));
        }
        [TestMethod]
        public void CalculateAreaTriangle()
        {
            Assert.AreEqual(20, Program.CalculateAreaTriangle(20, 2));
            Assert.AreNotEqual(15, Program.CalculateAreaTriangle(50, 27));
            Assert.AreEqual(16.25, Program.CalculateAreaTriangle(13, 2.5));
        }
    }
}
