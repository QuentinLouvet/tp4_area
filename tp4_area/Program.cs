﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp4_area
{
    public class Program
    {
        public static double CalculateAreaSquare(double Side)
        {
            double CalculatedAreaSquare;
            CalculatedAreaSquare = Side * Side;
            return CalculatedAreaSquare;
        }
        public static double CalculateAreaRectangle(double Length, double Width)
        {
            double CalculatedAreaRectangle;
            CalculatedAreaRectangle = Length * Width;
            return CalculatedAreaRectangle;
        }
        public static double CalculateAreaTriangle(double Base, double Height)
        {
            double CalculatedAreaTriangle;
            CalculatedAreaTriangle = (Base * Height) / 2;
            return CalculatedAreaTriangle;
        }
        static void Main(string[] args)
        {
        }
    }
}
